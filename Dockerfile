FROM node:alpine

RUN apk --update add git curl nginx yarn

COPY nginx.conf /etc/nginx/conf.d/site.conf
RUN mkdir -p /run/nginx

COPY . /code

WORKDIR /code

RUN yarn install
RUN yarn run build

RUN cp -r /code/build /home/node/app

WORKDIR /home/node/app

# Clean-up
RUN rm -rf /code

CMD ["nginx", "-g", "daemon off;"]
