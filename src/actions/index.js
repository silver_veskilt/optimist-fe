import * as types from '../constants/ActionTypes'
import config from '../config'

export const toggleNav = value => ({ type: types.TOGGLE_NAV, value })

export const resetProducts = () => ({ type: types.RESET_PRODUCTS })
export const receiveProducts = list => ({ type: types.RECEIVE_PRODUCTS, list })
export const addToCart = id => ({ type: types.ADD_TO_CART, id })
export const removeFromCart = id => ({ type: types.REMOVE_FROM_CART, id })
