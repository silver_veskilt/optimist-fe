import * as types from '../constants/ActionTypes'

export default (state = {
  open: false,
}, action) => {

  switch (action.type) {

    case types.TOGGLE_NAV:
      let open;

      if (action.value === true) open = true;
      else if (action.value === false) open = false;
      else open = !state.open;

      return { open }

    default:
      return state
  }
}
