import * as types from '../constants/ActionTypes'

export default (state = {
  initialized: false,
  products: [],
}, action) => {

  switch (action.type) {

    case types.RESET_PRODUCTS:
      return {
        initialized: false,
        products: [],
      };

    case types.RECEIVE_PRODUCTS:
      return {
        initialized: true,
        products: action.list,
      };

    case types.ADD_TO_CART:
    case types.REMOVE_FROM_CART:
      return {
        ...state,
        products: state.products.map(elem => (elem.id === action.id) ? product(elem, action) : elem),
      };

    default:
      return state
  }
}

const product = (state = {
  name:'unnamed',
  price: 0,
  amount: 0,
}, action) => {
  switch (action.type) {

    case types.ADD_TO_CART:
      return {
        ...state,
        amount: state.amount + 1,
      }
    case types.REMOVE_FROM_CART:
      if (state.amount === 0) return state; // do not go below 0
      return {
        ...state,
        amount: state.amount - 1,
      }

    default:
      return state
  }
}
