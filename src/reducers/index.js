import { combineReducers } from 'redux'
import nav from './nav'
import products from './products'

const rootReducer = combineReducers({
  products,
  nav,
})

export default rootReducer
