import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Cookies from 'js-cookie';
import { createCookieMiddleware } from 'redux-cookie';
import thunk from 'redux-thunk'
import reducer from './reducers'

import './materialize.css'
import './app.css'

import Nav from './containers/Nav'
import Products from './containers/Products'
import Cart from './containers/Cart'

const middleware = [ thunk, createCookieMiddleware(Cookies) ]

if (process.env.NODE_ENV !== 'production') {
  // middleware.push(createLogger())
}

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)

render(

  <Provider store={store}>

    <Router>
      <div>
        <Route path="/" component={Nav}/>
        <Route exact path="/" component={Products}/>
        <Route exact path="/cart" component={Cart}/>

      </div>
    </Router>

  </Provider>,
  document.getElementById('root')
)
