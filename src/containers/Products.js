import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as Actions from '../actions'
import { Button, Card, Row, Col, Icon, Input } from 'react-materialize';
import config from '../config'
import Axios from 'axios'

class Products extends Component {

  constructor (props) {
    super(props)

    // this.onProductClick = this.onProductClick.bind(this);
  }

  componentDidMount () {
    if (!this.props.products.initialized) this.loadContent();
  }

  async loadContent () {
    try {
      const { data } = await Axios.get(`${config.API_HOST}/products`)
      this.props.actions.receiveProducts(data);
    } catch (e) {

    }
  }

  onProductClick (id) {
    this.props.actions.addToCart(id);
  }

  render () {
    return <div className="center-align">

      <div className="container">

        <h1>Hi there!</h1>
        <h4>Choose a product</h4>

        <Row>
          <Col s={12} m={8} className="offset-m2">
            <ul className="collection left-align">
              { this.props.products.products.map(elem => <Link key={elem.id} to={'#'} onClick={e => this.onProductClick(elem.id)} className="collection-item">{ elem.name }<span className="right">{ elem.price } €</span></Link> )}
            </ul>
          </Col>

        </Row>

        <Row>
          <Link to="/cart" className="btn green white-text">Proceed to checkout</Link>
        </Row>
      </div>

    </div>
  }
}

Products.propTypes = {
  actions: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => ({ ...state })

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products)
