import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as Actions from '../actions'
import { Button, Card, Row, Col, Icon, Input } from 'react-materialize';
import config from '../config'

class Cart extends Component {

  constructor (props) {
    super(props)

    // important 0 - place for binding methods to container, will explain...
  }

  componentDidMount () {
    // important 1 - technically when-user-navigates-to-page(container) handler
  }

  componentWillReceiveProps(props){
    // important 2 - catch whatever props change, search query for example
  }

  onProductClick (id) {
    this.props.actions.removeFromCart(id);
  }

  onConfirmClick () {
    this.props.actions.resetProducts();
  }

  render () {

    const myProducts = this.props.products.products.filter(elem => elem.amount);
    let total = 0;
    myProducts.forEach(elem => {
      total = total + elem.amount * elem.price;
    })

    return <div className="center-align">

      <div className="container">

        <h1>Shopping Cart</h1>
        { myProducts.length ? <h4>Your items</h4> : <h4>Nothing is selected</h4>}

        <Row>
          <Col s={12} m={8} className="offset-m2">
            { !!myProducts.length && <div><ul className="collection left-align">
              { myProducts.map(elem => <Link key={elem.id} to={'#'} onClick={e => this.onProductClick(elem.id)} className="collection-item">
                { elem.name }

                <span className="right">{ elem.price }€</span>
                <span className="right">{ elem.amount } x&nbsp; </span>
                </Link> )}
            </ul>
              <h5>Total: {total.toFixed(2)}€ </h5>
            </div> }
          </Col>

        </Row>

        <Row>
          <Link to="/">Back</Link>
        </Row>
        <Row>
          { !!myProducts.length && <Link className="btn green white-text" to="/" onClick={e => this.onConfirmClick()}>Confirm</Link> }
        </Row>
      </div>

    </div>
  }
}

Cart.propTypes = {
  actions: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => ({ ...state })

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)
