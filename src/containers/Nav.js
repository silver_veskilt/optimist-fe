import React from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../actions'
import { Button, Card, Row, Col, Icon, Input, Navbar, NavItem } from 'react-materialize'
import { Link } from 'react-router-dom'

class Nav extends React.Component {

  constructor(props){
    super(props)
  }

  render() {
    const props = this.props;

    const myProducts = this.props.products.products.filter(elem => elem.amount);
    let itemsCount = 0;
    myProducts.forEach(elem => {
      itemsCount = itemsCount + elem.amount
    })

    return <div className="navbar">

      <nav className="z-depth-0">
       <div className="nav-wrapper grey darken-4 center-align">

         <Link to="#" className="button-collapse text-label" onClick={e => props.actions.toggleNav()}><Icon>view_headline</Icon></Link>

         <ul id="nav-mobile" className={`${props.nav.open && 'open'} side-nav right hide-on-large-only left-align`}>
           <li><Link className="text-label" to="/">Products</Link></li>
           <li><Link className="text-label" to="/cart">Cart</Link></li>
           <li><Link className="text-label" to="#" onClick={props.actions.toggleNav}><Icon>chevron_left</Icon></Link></li>
         </ul>

         <ul className="right hide-on-med-and-down">
           <li><Link className="text-label" to="/">Products</Link></li>
           <li><Link className="text-label" to="/cart">Cart {!!itemsCount && <span className="yellow-text">{`[${itemsCount}]`}</span>}</Link></li>
         </ul>
       </div>

       { props.nav.open && <div id="sidenav-overlay" className='hide-on-large-only' onClick={e => props.actions.toggleNav(false)}></div> }

      </nav>
    </div>
  }
}

Nav.propTypes = {
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => ({ ...state, ...ownProps })

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav)
