import React from 'react'
import { Link } from 'react-router-dom'
import { Button, Card, Row, Col, Icon, Input } from 'react-materialize';

export default (props) => <div className="navbar">

    <nav className="z-depth-0">
     <div className="nav-wrapper grey darken-4 grey-text text-darken-1 center-align">

       <Link to="#" className="button-collapse text-label" onClick={e => props.actions.toggleNav()}><Icon>view_headline</Icon></Link>

       <ul id="nav-mobile" className={`${props.nav.open && 'open'} side-nav right hide-on-large-only left-align`}>
         <li><Link className="text-label" to="/">Products</Link></li>
         <li><Link className="text-label" to="/cart">Cart</Link></li>
         <li><Link className="text-label" to="#" onClick={props.actions.toggleNav}><Icon>chevron_left</Icon></Link></li>
       </ul>

       <ul className="right hide-on-med-and-down">
         <li><Link className="text-label" to="/">Products</Link></li>
         <li><Link className="text-label" to="/cart">Cart</Link></li>
       </ul>
     </div>

     { props.nav.open && <div id="sidenav-overlay" className='hide-on-large-only' onClick={e => props.actions.toggleNav(false)}></div> }

    </nav>
  </div>
